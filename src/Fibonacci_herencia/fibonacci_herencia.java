package Fibonacci_herencia;

import java.util.Scanner;

class HelloThread extends Thread {

	public void run() {

		Scanner scan = new Scanner(System.in);
		int entrada, num, num2, i;
		do {
			System.out.print("Introduce un numero mayor que 1: ");
			entrada = scan.nextInt();
		} while (entrada <= 1);
		System.out.println("Los " + entrada
				+ " primeros t�rminos de la serie de Fibonacci son:");

		num = 1;
		num2 = 1;

		System.out.print(num + " ");
		for (i = 2; i <= entrada; i++) {
			System.out.print(num2 + " ");
			num2 = num + num2;
			num = num2 - num;
		}

	}

}

public class fibonacci_herencia {
	public static void main(String args[]) {
		new HelloThread().start();

	}
}
