package Fibonacci_interface;

import java.util.Scanner;

class hilo implements Runnable {
	Thread t;

	hilo() {
		t = new Thread(this, "Nuevo Thread");

		t.start(); // Arranca el nuevo hilo de ejecuci�n. Ejecuta run
	}

	public void run() {
		// C�digo a ejecutar por el hilo

		Scanner scan = new Scanner(System.in);
		int entrada, num, num2, i;
		do {
			System.out.print("Introduce un numero mayor que 1: ");
			entrada = scan.nextInt();
		} while (entrada <= 1);
		System.out.println("Los " + entrada
				+ " primeros t�rminos de la serie de Fibonacci son:");

		num = 1;
		num2 = 1;

		System.out.print(num + " ");
		for (i = 2; i <= entrada; i++) {
			System.out.print(num2 + " ");
			num2 = num + num2;
			num = num2 - num;
		}

	}

}

public class fibonacci {
	public static void main(String args[]) {
		new hilo(); // Crea un nuevo hilo de ejecuci�n

	}
}