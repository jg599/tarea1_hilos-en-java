package Controlador_Descontrolado;

class HelloThread extends Thread {

	public void run() {
		// C�digo a ejecutar por el hilo
		System.out.println("Hola desde el hilo creado!");
		int n = 0;
		do {
			System.out.println(n);
			n++;

		} while (n > 0);

	}
}

public class Controlador_Descontrolado {
	public static void main(String args[]) throws InterruptedException {

		HelloThread mithread = new HelloThread();
		mithread.start();

		System.out.println("Comienzo del contador descontrolado:");

		mithread.sleep(1000);

		mithread.stop();
		System.out.println("Ha terminado el contador");

	}
}